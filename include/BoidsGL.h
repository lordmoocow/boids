#ifndef BOIDSGL_H
#define BOIDSGL_H

#include <Manager.h>
#include <vector>

#include <SDL_GL.h>

namespace Boids
{
	/** Creates and manages a boids simulation using SDL and OpenGL. */
	class BoidsGL
	{
	private:
		/** Indicates whether the simulation is running */
		bool m_running;
		/** The display to which the simulation is rendered */
		SDL_Surface* m_display;
		/** Boid manager for the simulation */
		Manager m_manager;

		void GLContext();
		void EventListener();
		void Cleanup();

		/** @return bool True if initialisation is successful; otherwise false */
		bool Init();

		/** @param event The event that has been raised by SDL */
		void OnEvent(SDL_Event* event);

		void Update();
		void Draw() const;
	public:
		BoidsGL();
		int Execute();
	};
}

#endif // BOIDSGL_H
