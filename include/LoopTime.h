#ifndef LOOPTIME_H_
#define LOOPTIME_H_

#include <memory>
#include <ctime>

namespace Boids
{
	/** Tracks the time for a loop to complete */
	class LoopTime
	{
		private:
			/** The target time for each loop */
			float m_target_frame_time = (CLOCKS_PER_SEC/60);

			/** The time the loops started */
			std::clock_t m_loop_start;
			/** The time the loop ended */
			std::clock_t m_loop_end;

			/** The time spent on the last loop */
			float m_last_loop_time;
		public:
			LoopTime() : m_loop_end(std::clock()) {}
			LoopTime(unsigned int framerate) : m_loop_end(std::clock())
			{
				m_target_frame_time = (CLOCKS_PER_SEC/framerate);
			}

			const float DeltaTime();
			const float ElapsedTime();

			const bool FrameLimit();

			void StartLoop();
			void EndLoop();
	};

	typedef std::shared_ptr<LoopTime> loopTime_ptr;

	/** Gets the time taken since the previous loop started
	 * @return float The time in seconds since the start of the previous loop */
	inline const float LoopTime::DeltaTime()
	{
		//division by CLOCKS_PER_SEC converts the value to seconds
		return (ElapsedTime() + m_last_loop_time) / CLOCKS_PER_SEC;
	}

	/** Gets the time elapsed since the end of the previous loop
	 * @return float The time in milliseconds since the previous loop ended */
	inline const float LoopTime::ElapsedTime()
	{
		return std::clock() - m_loop_end;
	}

	/** Restricts the loop to run no more than once per target time frame
	 * @return bool True if the minimum time for the loop has been exceeded; otherwise false*/
	inline const bool LoopTime::FrameLimit()
	{
		return (ElapsedTime() + m_last_loop_time) > m_target_frame_time;
	}

	/** Marks the end of the loop */
	inline void LoopTime::EndLoop()
	{
		m_loop_end = std::clock();
		m_last_loop_time = (m_loop_end - m_loop_start);
	}

	/** Marks the start of the loop */
	inline void LoopTime::StartLoop()
	{
		m_loop_start = std::clock();
	}
}

#endif // LOOPTIME_H_
