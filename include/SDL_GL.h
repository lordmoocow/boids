#ifndef SDL_GL_H_
#define SDL_GL_H_

#include <SDL.h>
#include <SDL/SDL_opengl.h>
#include <GL/gl.h>
#include <GL/glu.h>

#endif // SDL_GL_H_
