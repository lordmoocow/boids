#ifndef DRAWABLE_H_
#define DRAWABLE_H_

#include <LoopTime.h>

namespace Boids
{
	/** Prescribes a Draw method */
	class Drawable
	{
	public:
		/** @param renderTime Tracks the time elapsed on render cycles */
		virtual void Draw(loopTime_ptr renderTime) const = 0;
		virtual ~Drawable() {};
	};
}

#endif // DRAWABLE_H_
