#ifndef AXISENUM_H_
#define AXISENUM_H_

namespace Boids
{
	/** Axis array index */
	enum Axis
	{
		X = 0,
		x = 0,
		Y = 1,
		y = 1,
		Z = 2,
		z = 2
	};
}

#endif // AXISENUM_H_
