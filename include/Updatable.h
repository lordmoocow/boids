#ifndef UPDATABLE_H_
#define UPDATABLE_H_

#include <LoopTime.h>

namespace Boids
{
	/** Prescribes an Update method */
	class Updatable
	{
	public:
		/** @param updateTime Tracks the time elapsed on update cycles */
		virtual void Update(loopTime_ptr updateTime) = 0;
		virtual ~Updatable() {};
	};
} /* namespace Boids */

#endif /* UPDATABLE_H_ */
