#ifndef BOID_H_
#define BOID_H_

#include <Updatable.h>
#include <Drawable.h>
#include <Point.h>
#include <Rule.h>

namespace Boids
{
	class Rule;
	/** A bird-like object subject to rules of it's manager */
	class Boid: public Updatable, public Drawable
	{
	private:
		/** The current position of the boid */
		Point m_position;
		/** The current velocity of the boid */
		Point m_velocity;

		void Move(float dt);
	public:
		Boid();
		Boid(Point position);
		Boid(Point position, Point velocity);

		const Point& GetPosition();
		const Point& GetVelocity();

		/** @param rules The rules to apply to this boid
		 * @param updateTime The update loop timer */
		void ApplyRules(std::shared_ptr<std::vector<std::shared_ptr<Rule>>> rules, loopTime_ptr updateTime);

		/** @param updateTime The update loop timer */
		virtual void Update(loopTime_ptr updateTime);
		/** @param renderTime The render loop timer */
		virtual void Draw(loopTime_ptr renderTime) const;
	};

	inline const Point& Boid::GetPosition()
	{
		return m_position;
	}

	inline const Point& Boid::GetVelocity()
	{
		return m_velocity;
	}

} /* namespace Boids */

#endif /* BOID_H_ */
