#ifndef MANAGER_H
#define MANAGER_H

#include <Updatable.h>
#include <Drawable.h>
#include <Boid.h>
#include <Rule.h>

#include <vector>

namespace Boids
{
	/** Singleton; Manages a flock of boids with a set of rules */
	class Manager : public Updatable, public Drawable
	{
		private:
			/** The dimensions of the simulation area */
			Point m_simulationDimensions;

			/** Collection of boids to be managed */
			std::vector<Boid> m_boids;
			/** Collection of rules to apply */
			std::vector<std::shared_ptr<Rule>> m_rules;

			/** @param numBoids The number of boids to generate */
			void GenerateBoids(unsigned int numBoids);

			void AddRules();
		public:
			/** @param dimensions The simulation area width, height and depth
			 * @param numBoids The number of boids to generate for the simulation */
			Manager(Point dimensions, unsigned int numBoids);

			/** @param updateTime Tracks the time elapsed for update cycles */
			void Update(loopTime_ptr updateTime);

			/** @param renderTime Tracks the time elapsed for render cycles */
			void Draw(loopTime_ptr renderTime) const;
	};
}

#endif // MANAGER_H
