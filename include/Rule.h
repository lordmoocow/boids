#ifndef RULE_H
#define RULE_H

#include <Boid.h>
#include <vector>
#include <memory>

namespace Boids
{
	class Boid;
	/** Abstract rule class, describes execution and construction of derivative rules */
	class Rule
	{
		protected:
			typedef std::shared_ptr<std::vector<Boid>> boids_ptr;
			/** The boids that this rules manager is managing */
			boids_ptr m_boids;
			std::shared_ptr<Point> m_bounds;

			Rule(boids_ptr boids) : Rule(boids, nullptr) {}
			Rule(boids_ptr boids, std::shared_ptr<Point> bounds) : m_boids(boids) , m_bounds(bounds) {}
		public:
			virtual Point Execute(std::shared_ptr<Boid> boid, loopTime_ptr updateTime)=0;
			virtual ~Rule() {}
	};

}

#endif // RULE_H
