#ifndef POINT_H_
#define POINT_H_

#include <vector>
#include <initializer_list>
#include <math.h>
#include <float.h>
#include <AxisEnum.h>

namespace Boids
{
	class Point
	{
	private:
		std::vector<float> m_vec3;
	public:
		Point();
		Point(std::initializer_list<float> args);

		float& operator[](const Axis& index);
		const float& operator[](const Axis& index) const;

		friend Point operator+(const Point& A, const Point& B);
		friend Point operator-(const Point& A, const Point& B);
		friend Point operator/(const Point& A, const float& divisor);
		friend Point operator*(const Point& A, const float& multiplier);

		Point& operator+=(const Point& B);
		Point& operator-=(const Point& B);
		Point& operator/=(const float& divisor);
		Point& operator*=(const float& multiplier);

		const float Length();
		void Normalize();
	};

	inline Point::Point()
	{
		m_vec3 = {0,0,0};
	}

	inline Point::Point(std::initializer_list<float> args)
	{
		if(args.size() > 3) throw std::invalid_argument("cannot have mroe than 3 elements");
		for(auto a : args)
			m_vec3.emplace_back(a);
	}

	inline float& Point::operator[](const Axis& index)
	{
		return this->m_vec3[index];
	}

	inline const float& Point::operator[](const Axis& index) const
	{
		return this->m_vec3[index];
	}

	inline Point operator+(const Point& A, const Point& B)
	{
		return Point{A[X]+B[X],A[Y]+B[Y],A[Z]+B[Z]};
	}

	inline Point& Point::operator+=(const Point& B)
	{
		(*this)[X] += B[X];
		(*this)[Y] += B[Y];
		(*this)[Z] += B[Z];
		return *this;
	}

	inline Point operator-(const Point& A, const Point& B)
	{
		return Point{A[X]-B[X],A[Y]-B[Y],A[Z]-B[Z]};
	}

	inline Point& Point::operator-=(const Point& B)
	{
		(*this)[X] -= B[X];
		(*this)[Y] -= B[Y];
		(*this)[Z] -= B[Z];
		return *this;
	}

	inline Point operator/(const Point& A, const float& divisor)
	{
		return Point{A[X]/divisor,A[Y]/divisor,A[Z]/divisor};
	}

	inline Point& Point::operator/=(const float& divisor)
	{
		(*this)[X] /= divisor;
		(*this)[Y] /= divisor;
		(*this)[Z] /= divisor;
		return *this;
	}

	inline Point operator*(const Point& A, const float& multiplier)
	{
		return Point{A[X]*multiplier,A[Y]*multiplier,A[Z]*multiplier};
	}

	inline Point& Point::operator*=(const float& multiplier)
	{
		(*this)[X] *= multiplier;
		(*this)[Y] *= multiplier;
		(*this)[Z] *= multiplier;
		return *this;
	}


	inline const float Point::Length()
	{
		return sqrt(  ( (*this)[X] * (*this)[X] )
					+ ( (*this)[Y] * (*this)[Y] )
					+ ( (*this)[Z] * (*this)[Z] ) );
	}

	inline void Point::Normalize()
	{
		float length = this->Length();
		if((length - 0) > FLT_EPSILON)
			*this = *this / length;
	}
}

#endif // POINT_H_
