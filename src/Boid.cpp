#include <Boid.h>
#include <utility>
#include <numeric>
#include <SDL_GL.h>

namespace Boids
{
	/** Default constructor */
	Boid::Boid()
	: m_position(Point{0,0,0})
	, m_velocity(Point{0,0,0})
	{}

	/** Constructs a boid at a specified position */
	Boid::Boid(Point position)
	: m_position(std::move(position))
	, m_velocity(Point{0,0,0})
	{}

	/** Constructs a boid at a position with velocity */
	Boid::Boid(Point position, Point velocity)
	: m_position(std::move(position))
	, m_velocity(std::move(velocity))
	{}

	/** Moves the boid using the current velocity */
	void Boid::Move(float dt)
	{
		//alter the position of the boid using the calculated delta velocity
		m_position += (m_velocity * dt);
	}

	/** Apply each of the rules provided by the manager to this boid */
	void Boid::ApplyRules(std::shared_ptr<std::vector<std::shared_ptr<Rule>>> rules, loopTime_ptr updateTime)
	{
		for(auto& rule : *rules)
		{
			//add the result of the rules execution to the boids velocity
			m_velocity += rule->Execute(std::make_shared<Boid>(*this), updateTime);
		}

		auto length = m_velocity.Length();
		if(length > 45)
		{
			m_velocity.Normalize();
			m_velocity *= 45;
		}
	}

	/** The update logic of the boid */
	void Boid::Update(loopTime_ptr updateTime)
	{
		Move(updateTime->DeltaTime());
	}

	/** Draws the boid on the display */
	void Boid::Draw(loopTime_ptr renderTime) const
	{
		glColor3f(0,1,0);
		glVertex3f(m_position[X]-1, m_position[Y]-1, m_position[Z]);
		glVertex3f(m_position[X]+1, m_position[Y]-1, m_position[Z]);
		glVertex3f(m_position[X]+1, m_position[Y]+1, m_position[Z]);
		glVertex3f(m_position[X]-1, m_position[Y]+1, m_position[Z]);
	}
} /* namespace Boids */
