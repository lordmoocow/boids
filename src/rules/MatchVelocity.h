#ifndef MATCHVELOCITY_H
#define MATCHVELOCITY_H

#include <Rule.h>

namespace Boids
{
	/** Aligns boids to the same direction and speed */
	class MatchVelocity : public Rule
	{
		public:
			MatchVelocity(boids_ptr boids) : Rule(boids){};
			virtual Point Execute(std::shared_ptr<Boid> boid, loopTime_ptr ticks);
	};

	inline Point MatchVelocity::Execute(std::shared_ptr<Boid> boid, loopTime_ptr updateTime)
	{
		Point result = Point{0,0,0};

		for(auto& other : *m_boids)
		{
			if(&other != boid.get())
				result += other.GetVelocity();
		}

		result /= (m_boids->size()-1);
		result -= boid->GetVelocity();
		result /= 25;
		return result;
	}
}

#endif // MATCHVELOCITY_H
