#ifndef CENTRALMASS_H
#define CENTRALMASS_H

#include <Rule.h>

namespace Boids
{
	/** Directs a boid towards a centre of mass */
	class CentralMass : public Rule
	{
		public:
			CentralMass(boids_ptr boids) : Rule(boids){};
			virtual Point Execute(std::shared_ptr<Boid> boid, loopTime_ptr updateTime);
	};

	inline Point CentralMass::Execute(std::shared_ptr<Boid> boid, loopTime_ptr updateTime)
	{
		Point result = Point{0,0,0};

		for(auto& other : *m_boids)
		{
			if(&other != boid.get())
				result += other.GetPosition();
		}

		result /= (m_boids->size()-1);
		result -= boid->GetPosition();
		result /= 200;
		return result;
	}
}

#endif // CENTRALMASS_H
