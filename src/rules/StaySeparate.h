#ifndef STAYSEPARATE_H
#define STAYSEPARATE_H

#include <Rule.h>

namespace Boids
{
	/** Prevents boids from colliding */
	class StaySeparate : public Rule
	{
		public:
			StaySeparate(boids_ptr boids) : Rule(boids) {};
			virtual Point Execute(std::shared_ptr<Boid> boid, loopTime_ptr ticks);
	};

	inline Point StaySeparate::Execute(std::shared_ptr<Boid> boid, loopTime_ptr updateTime)
	{
		Point result = Point{0,0,0};
		auto pos = boid->GetPosition();

		for(auto& other : *m_boids)
		{
			if(&other != boid.get())
			{
				auto distance = (other.GetPosition() - pos);
				if(distance.Length() < 200)
					result -= distance;
			}
		}

		return result;
	}
}

#endif // STAYSEPARATE_H
