#ifndef WITHINBOUNDS_H_
#define WITHINBOUNDS_H_

#include <Rule.h>

namespace Boids
{
	/** Keeps boids within the bounadaries of the simulation */
	class WithinBounds : public Rule
	{
		public:
			WithinBounds(boids_ptr boids, std::shared_ptr<Point> bounds) : Rule(boids, bounds){};
			virtual Point Execute(std::shared_ptr<Boid> boid, loopTime_ptr updateTime);
	};

	inline Point WithinBounds::Execute(std::shared_ptr<Boid> boid, loopTime_ptr updateTime)
	{
		Point result = Point{0,0,0};
		auto& pos = boid->GetPosition();

		if(pos[X] < -((*m_bounds)[X]/2))
			result[X] = 15;
		else if(pos[X] > (*m_bounds)[X]/2)
			result[X] = -15;

		if(pos[Y] < -((*m_bounds)[Y]/2))
			result[Y] = 15;
		else if(pos[Y] > (*m_bounds)[Y]/2)
			result[Y] = -15;

		if(pos[Z] < -((*m_bounds)[Z]))
			result[Z] = 15;
		else if(pos[Z] > 0)
			result[Z] = -15;

		return result;
	}
}

#endif // WITHINBOUNDS_H_
