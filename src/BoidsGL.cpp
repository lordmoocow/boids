#include "BoidsGL.h"
#include <thread>

namespace Boids
{
	/**
	 * Default constructor
	 */
	BoidsGL::BoidsGL()
	: m_running(true)
	, m_display(NULL)
	, m_manager(Point{1024,768,1024}, 500)
	{}

	/**
	 * Begins execution and rendering of the simulation
	 * @return int Exit status code; <0 indicates error
	 */
	int BoidsGL::Execute()
	{
		//launch threads for rendering and updating
		std::vector<std::thread> threads;
		threads.push_back(std::thread(&BoidsGL::GLContext, this));
		threads.push_back(std::thread(&BoidsGL::Update, this));

		//now that rendering and updating is delgated into seperate threads
		//we can wait here until they are return
		for(auto& thread : threads)
			thread.join();

		return 0;
	}

	/**
	 * Creates an OpenGL context and starts listening for events and rendering.
	 */
	void BoidsGL::GLContext()
	{
		//Initiliase SDL/OpenGL
		//if this fails for any reason, stop running and let the application close down
		m_running = Init();

		//launch a separate thread to listen concurrently for events raised by SDL
		std::thread event(&BoidsGL::EventListener, this);

		//draw until m_running is false
		Draw();

		//wait for the event listener thread to return
		event.join();

		//bring everything to a tidy close
		Cleanup();
	}

	/**
	 * Poll for events raised by SDL until m_running is false.
	 */
	void BoidsGL::EventListener()
	{
		SDL_Event event;
		while(m_running)
		{
			while(SDL_PollEvent(&event))
				OnEvent(&event);
		}
	}

	/**
	 * Quits SDL and frees up any data which is not managed elsewhere
	 */
	void BoidsGL::Cleanup()
	{
		SDL_Quit();
	}

	/**
	 * Initialises the SDL/OpenGL viewport
	 * @return bool True if initialisation is successful; otherwise false
	 */
	bool BoidsGL::Init()
	{
		//initialise SDL, if this returns negative it is a failure
		if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
			return false;

		//set up the display configuration
		m_display = SDL_SetVideoMode(1024, 1024, 32,
									SDL_HWSURFACE //use hardware accelleration
									| SDL_GL_DOUBLEBUFFER //double buffer using opengl
									| SDL_OPENGL); //use opengl
		//ensure that the display initialised correctly,
		//this will be a null ptr if not
		if(m_display == NULL)
			return false;

		//configure viewport
		glClearColor(0,0,0,0);
		glViewport(0, 0, 1024, 1024);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		//configure camera
		gluPerspective(90, 1024/1024, 0.001f, 1024);
		glMatrixMode(GL_MODELVIEW);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);
		glLoadIdentity();

		//therefore if nothing has gone wrong above...
		return true;
	}

	/**
	 * Handle an SDL event that has occurred
	 * @param event The event that has been raised by SDL
	 */
	void BoidsGL::OnEvent(SDL_Event* event)
	{
		switch(event->type)
		{
		case SDL_QUIT:
			m_running = false;
			break;
		}
	}

	/**
	 * Updates all of the elements until m_running is false
	 */
	void BoidsGL::Update()
	{
		auto updateTime = loopTime_ptr(new LoopTime(20));
		while(m_running)
		{
			if(updateTime->FrameLimit())
			{
				updateTime->StartLoop();

				m_manager.Update(updateTime);

				updateTime->EndLoop();
			}
		}
	}

	/**
	 * Clears the screen and draws all the elements of the next
	 * frame before swapping the buffers.
	 */
	void BoidsGL::Draw() const
	{
		auto renderTime = loopTime_ptr(new LoopTime(60));
		while(m_running)
		{
			if(renderTime->FrameLimit())
			{
				renderTime->StartLoop();

				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				glLoadIdentity();

				//TODO: draw entities
				glBegin(GL_QUADS);
				m_manager.Draw(renderTime);
				glEnd();

				SDL_GL_SwapBuffers();

				renderTime->EndLoop();
			}
		}
	}
}
