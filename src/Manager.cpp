#include "Manager.h"
#include <utility>
#include <random>
#include "rules/AllRules.h"

namespace Boids
{
	/** Default constructor */
	Manager::Manager(Point dimensions, unsigned int numBoids)
	: m_simulationDimensions(std::move(dimensions))
	{
		m_boids.reserve(numBoids);
		GenerateBoids(numBoids);
		AddRules();
	}

	/** Generates a number of boids in random positions within the simulation area */
	void Manager::GenerateBoids(unsigned int numBoids)
	{
		//prepare randum number ranges for each axis and velocity
		typedef std::uniform_real_distribution<float> dist;
		std::mt19937_64 engine;
		engine.seed(std::random_device{}());
		dist x_distribution (-(m_simulationDimensions[X]/2), m_simulationDimensions[X]/2);
		dist y_distribution (-(m_simulationDimensions[Y]/2), m_simulationDimensions[Y]/2);
		dist z_distribution (0, -m_simulationDimensions[Z]/2);
		dist v_distribution (-15, 15);

		//generate the boids; adding them to the list
		for(unsigned int i=0; i<numBoids; ++i)
			m_boids.push_back(
				Boid(
					Point //random position
					{
						x_distribution(engine),
						y_distribution(engine),
						z_distribution(engine)
					},
					Point //random velocity
					{
						v_distribution(engine),
						v_distribution(engine),
						v_distribution(engine)
					}
				)
			);
	}

	/** Adds the rules to beused in the simulation to the manager */
	void Manager::AddRules()
	{
		auto boids = std::make_shared<std::vector<Boid>>(m_boids);
		m_rules.emplace_back(std::make_shared<WithinBounds>(boids, std::make_shared<Point>(m_simulationDimensions)));
		m_rules.emplace_back(std::make_shared<CentralMass>(boids));
		m_rules.emplace_back(std::make_shared<StaySeparate>(boids));
		m_rules.emplace_back(std::make_shared<MatchVelocity>(boids));
	}

	/** Execute all the rules and update all of the boids being managed */
	void Manager::Update(loopTime_ptr updateTime)
	{
		for(auto& boid : m_boids)
		{
			boid.ApplyRules(std::make_shared<std::vector<std::shared_ptr<Rule>>>(m_rules), updateTime);
			boid.Update(updateTime);
		}
	}

	/** Draw all of the boids */
	void Manager::Draw(loopTime_ptr renderTime) const
	{
		for(const auto& boid : m_boids)
			boid.Draw(renderTime);
	}
}
